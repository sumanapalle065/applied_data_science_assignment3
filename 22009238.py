
# -*- coding: utf-8 -*-
"""
Created on Thu May 11 21:01:52 2023

@author: sumana
"""

#importing the required libraries
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import numpy as np
from scipy.optimize import curve_fit


def read_csv_file(file_path):
    df = pd.read_csv(file_path, header=2)
    return df


def scaler(df):
    """ Expects a dataframe and normalises all 
        columns to the 0-1 range. It also returns 
        dataframes with minimum and maximum for
        transforming the cluster centres"""

    # Uses the pandas methods
    df_min = df.min()
    df_max = df.max()

    df = (df-df_min) / (df_max - df_min)

    return df, df_min, df_max


def backscale(arr, df_min, df_max):
    """ Expects an array of normalised cluster centres and scales
        it back. Returns numpy array.  """

    # convert to dataframe to enable pandas operations
    minima = df_min.to_numpy()
    maxima = df_max.to_numpy()
    for i in range(len(minima)):
        arr[:, i] = arr[:, i] * (maxima[i] - minima[i]) + minima[i]

    return arr

def find_optimal_clusters(data, max_clusters=10):
    """Find the optimal number of clusters for KMeans clustering using the elbow method.

    Args:
    data: numpy array or pandas dataframe, the data to cluster
    max_clusters: int, the maximum number of clusters to consider

    Returns:
    None
    """
    wcss = []
    for i in range(1, max_clusters+1):
        kmeans = KMeans(n_clusters=i, init='k-means++', max_iter=300, n_init=10, random_state=0)
        kmeans.fit(data)
        wcss.append(kmeans.inertia_)

    plt.plot(range(1, max_clusters+1), wcss)
    plt.title('Elbow Method')
    plt.xlabel('Number of clusters')
    plt.ylabel('WCSS')
    plt.show()

def kmeans_clustering(df_norm, df_years, df_min, df_max, optimal_clusters=3):
    # Create KMeans object
    kmeans = KMeans(n_clusters=optimal_clusters, init='k-means++',
                    max_iter=300, n_init=10, random_state=0)

    # Perform KMeans clustering
    df_years['Cluster'] = kmeans.fit_predict(df_norm)

    # Add cluster classification as a new column to the dataframe
    df_years['Cluster'] = kmeans.labels_

    # Set up a color map for the clusters
    cmap = plt.get_cmap('viridis')

    # Plot the clustering results
    plt.figure(figsize=(12, 8))
    for i in range(optimal_clusters):
        # Select the data for the current cluster
        cluster_data = df_years[df_years['Cluster'] == i]
        # Get the color for the current cluster
        color = cmap(i / (optimal_clusters - 1))
        # Plot the data
        plt.scatter(cluster_data.index, cluster_data['2000'], label=f'Cluster {i}', color=color)

    # Plot the cluster centers
    cluster_centers = backscale(kmeans.cluster_centers_, df_min, df_max)
    for i in range(optimal_clusters):
        # Get the color for the current cluster
        color = cmap(i / (optimal_clusters - 1))
        # Plot the center for the current cluster
        plt.scatter(len(df_years), cluster_centers[i, -1],
                    marker='+', s=150, c='black', label=f'Cluster Center {i}')

    # Set the title and axis labels
    plt.title('Clusters for CO2 emissions')
    plt.xlabel('Country Index')
    plt.ylabel('CO2 emissions (metric tons per capita) in 2000')
    plt.legend()
    plt.show()

    #Displaying the list of countries in each cluster
    country_clusters = []
    for i in range(optimal_clusters):
        cluster_countries = df_years[df_years['Cluster']
                                     == i][['Country Name', 'Country Code']]
        cluster_countries['Cluster'] = f'Cluster {i}'
        country_clusters.append(cluster_countries)

    # Concatenate all the dataframes
    all_countries = pd.concat(country_clusters)

    # Group the dataframe by cluster
    grouped = all_countries.groupby('Cluster')

    # Display the result
    for i, group in grouped:
        print(f'Countries in Cluster {i}:')
        print(group)
        print()


#curvefitting
def plot_country_emissions(df, country):
    # Define the linear model
    def linear_model(x, a, b):
        return a * x + b

    # Define the function for calculating error ranges
    def err_ranges(popt, pcov, x):
        perr = np.sqrt(np.diag(pcov))
        y = linear_model(x, *popt)
        lower = linear_model(x, *(popt - perr))
        upper = linear_model(x, *(popt + perr))
        return y, lower, upper

    # Define the columns to use
    columns_to_use = [str(year) for year in range(1960, 2000)]

    # Extract data for the selected country
    df_country = df.loc[df['Country Name'] == country]
    country_data = df_country[columns_to_use].values[0]
    x_data = np.array(range(1960, 2000))
    y_data = np.nan_to_num(country_data)

    # Fit the linear model
    popt, pcov = curve_fit(linear_model, x_data, y_data)

    # Predict future values and confidence intervals
    x_future = np.array(range(1960, 2050))
    y_future, lower_future, upper_future = err_ranges(popt, pcov, x_future)

    # Plot the results
    plt.figure(figsize=(12, 6))
    plt.plot(x_data, y_data, 'o', label='Data', color='#D14D72')
    plt.plot(x_future, y_future, '-', label='Best Fit', color='black')
    plt.fill_between(x_future, lower_future, upper_future,
                     alpha=0.38, label='Confidence Interval', color='#FCC8D1')
    plt.xlabel('Year')
    plt.ylabel('CO2 emissions (metric tons per capita)')
    plt.title(f'{country} CO2 emissions Curve Fitting')
    plt.legend()
    plt.show()

# comparison of countries in diffrent clusters
def plot_co2_emission_l(country1, country2, start_year, end_year):
    df_country1 = df[df['Country Name'] == country1]
    df_country2 = df[df['Country Name'] == country2]

    # Get the electric consumption data for the specified years
    years = [str(year) for year in range(start_year, end_year + 1)]
    emissions1 = [df_country1[year].values[0] for year in years]
    emissions2 = [df_country2[year].values[0] for year in years]

    # Create the line chart
    plt.plot(years, emissions1, marker='o', color='#5F264A', label=country1)
    plt.plot(years, emissions2, marker='o', color='#FF6000', label=country2)
    plt.title(f'{country1} and {country2}: CO2 emissions (metric tons per capita) from {start_year} to {end_year}')
    plt.xlabel('Year')
    plt.ylabel('CO2 emissions (metric tons per capita)')
    plt.legend()
    plt.show()


file_path = r"C:\Users\navee\Downloads\API_EN.ATM.CO2E.PC_DS2_en_csv_v2_5455265.csv"
df = read_csv_file(file_path)
  
# Selecting the columns to be used for clustering
columns_to_use = [str(year) for year in range(1960, 2010)]
df_years = df[['Country Name', 'Country Code'] + columns_to_use]

# Fill missing values with the mean
df_years = df_years.fillna(df_years.mean())

# Normalize the data
df_norm, df_min, df_max = scaler(df_years[columns_to_use])
df_norm.fillna(0, inplace=True)  # replace NaN values with 0

find_optimal_clusters(df_norm)
#k-means clustering
kmeans_clustering(df_norm, df_years, df_min, df_max, optimal_clusters=3)
plot_country_emissions(df_years, 'Bahrain')
plot_co2_emission_l('Bahrain', 'Poland', 2000, 2010)
